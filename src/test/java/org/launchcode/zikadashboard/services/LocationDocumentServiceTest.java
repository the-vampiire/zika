package org.launchcode.zikadashboard.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.launchcode.zikadashboard.models.DTOs.LocationFeatureCollection;
import org.launchcode.zikadashboard.models.LocationDocument;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

@IntegrationTestConfig
class LocationDocumentServiceTest {
  @Autowired
  private LocationDocumentService locationDocumentService;

  @Test
  @DisplayName(
      "search(LocationDocument.SearchTerm.country, value): returns a LocationFeatureCollection " + "for a " + "search by country")
  void searchByCountry() {
    LocationFeatureCollection locationFeatureCollection = locationDocumentService
        .search(LocationDocument.SearchTerm.country,
                IntegrationTestConfig.KNOWN_COUNTRY_SEARCH_VALUE
        );

    assertEquals(IntegrationTestConfig.FEATURES_COUNT_FOR_KNOWN_COUNTRY_SEARCH,
                 locationFeatureCollection.getFeatures().size()
    );
  }

  @Test
  @DisplayName(
      "search(LocationDocument.SearchTerm.state, value): returns a LocationFeatureCollection for "
          + "a " + "search by state")
  void searchByState() {
    LocationFeatureCollection locationFeatureCollection = locationDocumentService
        .search(LocationDocument.SearchTerm.state, IntegrationTestConfig.KNOWN_STATE_SEARCH_VALUE);

    assertEquals(IntegrationTestConfig.FEATURES_COUNT_FOR_KNOWN_STATE_SEARCH,
                 locationFeatureCollection.getFeatures().size()
    );
  }
}