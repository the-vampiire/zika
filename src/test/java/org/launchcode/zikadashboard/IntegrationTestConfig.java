package org.launchcode.zikadashboard;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IntegrationTestConfig {
  // reports by date
  String KNOWN_REPORTS_DATE = "2015-11-28";
  String UNKNOWN_REPORTS_DATE = "1965-03-01";
  int REPORTS_COUNT_FOR_KNOWN_DATE = 84;
  int FEATURES_COUNT_FOR_KNOWN_DATE = 34;
  // known report
  long KNOWN_REPORT_ID = 1119L;
  // report dates
  int DISTINCT_REPORT_DATES_COUNT = 253;
  // known location
  long KNOWN_LOCATION_ID = 25L;
  int REPORTS_COUNT_FOR_KNOWN_LOCATION = 365;
  // elasticsearch location doc searching
  String KNOWN_COUNTRY_SEARCH_VALUE = "columbia";
  int FEATURES_COUNT_FOR_KNOWN_COUNTRY_SEARCH = 23;
  String KNOWN_STATE_SEARCH_VALUE = "santa";
  int FEATURES_COUNT_FOR_KNOWN_STATE_SEARCH = 3;
}
