package org.launchcode.zikadashboard.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfig
class ReportsControllerTest {
  @Autowired
  private MockMvc mockMvc;

  private String buildReportByDatePath(String date) {
    return String.format("%s?date=%s", ReportsController.ROOT_ENDPOINT, date);
  }

  @Test
  @DisplayName("GET /reports?date={known date}: List of Reports corresponding to the date")
  void getReportsByKnownDate() throws Exception {
    String reportsByDate = buildReportByDatePath(IntegrationTestConfig.KNOWN_REPORTS_DATE);

    mockMvc.perform(get(reportsByDate)).andExpect(status().isOk()).andExpect(
        jsonPath("$.features.length()").value(IntegrationTestConfig.FEATURES_COUNT_FOR_KNOWN_DATE));
  }

  @Test
  @DisplayName("GET /reports?date={unknown date}: empty list")
  void getReportsByUnknownDate() throws Exception {
    String reportsByDate = buildReportByDatePath(IntegrationTestConfig.UNKNOWN_REPORTS_DATE);

    mockMvc.perform(get(reportsByDate)).andExpect(status().isOk())
           .andExpect(jsonPath("$.features.length()").value(0));
  }

  @Test
  @DisplayName("GET /reports?date={invalid date format}: 400 response")
  void getReportsWithInvalidDateFormat() throws Exception {
    String reportsByDateInvalid = buildReportByDatePath("some date");

    mockMvc.perform(get(reportsByDateInvalid)).andExpect(status().is4xxClientError());
  }

  @Test
  @DisplayName("GET /reports/dates: list of distinct report date Strings")
  void getReportDates() throws Exception {
    String datesEndpoint = String
        .format("%s%s", ReportsController.ROOT_ENDPOINT, ReportsController.DISTINCT_DATES_PATH);

    mockMvc.perform(get(datesEndpoint)).andExpect(status().isOk()).andExpect(
        jsonPath("$.length()").value(IntegrationTestConfig.DISTINCT_REPORT_DATES_COUNT));
  }
}