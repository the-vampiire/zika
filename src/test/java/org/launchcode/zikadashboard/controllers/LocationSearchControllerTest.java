package org.launchcode.zikadashboard.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.launchcode.zikadashboard.models.LocationDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfig
class LocationSearchControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @Test
  @DisplayName("GET /reports/search: restricts search term only to enum term values")
  void searchByInvalidTerm() throws Exception {
    String searchEndpoint = String
        .format("%s?term=notEnum&value=somevalue", LocationSearchController.ROOT_ENDPOINT);

    mockMvc.perform(get(searchEndpoint)).andExpect(status().is4xxClientError());
  }

  @Test
  @DisplayName(
      "GET /reports/search/terms: returns a serialized list of LocationDocument.SearchTerm " +
          "enum values")
  void searchTerms() throws Exception {
    String searchTermsEndpoint = String.format("%s/%s", LocationSearchController.ROOT_ENDPOINT,
                                               LocationSearchController.SEARCH_TERMS_PATH
    );

    mockMvc.perform(get(searchTermsEndpoint)).andExpect(status().isOk())
           .andExpect(jsonPath("$.length()").value(LocationDocument.SearchTerm.values().length));
  }
}