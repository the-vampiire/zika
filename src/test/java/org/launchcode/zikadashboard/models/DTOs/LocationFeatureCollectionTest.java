package org.launchcode.zikadashboard.models.DTOs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.launchcode.zikadashboard.data.LocationRepository;
import org.launchcode.zikadashboard.data.ReportRepository;
import org.launchcode.zikadashboard.models.Location;
import org.launchcode.zikadashboard.models.Report;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@IntegrationTestConfig
class LocationFeatureCollectionTest {
  @Autowired
  private ReportRepository reportRepository;

  @Autowired
  private LocationRepository locationRepository;

  private List<Report> reports = null;

  @BeforeEach
  void setup() {
    // cache reports (not changing) for use in fromReports and GeoJSON serialization tests
    if (reports != null) return;

    reports = reportRepository
        .findAllByReportDate(Date.valueOf(IntegrationTestConfig.KNOWN_REPORTS_DATE));
  }

  @Test
  @DisplayName(
      "static fromLocations(): creates a LocationFeatureCollection with features from each " +
          "Location")
  void fromLocations() {
    List<Location> locations = locationRepository.findAll();
    LocationFeatureCollection locationFeatureCollection = LocationFeatureCollection
        .fromLocations(locations);

    assertEquals(locations.size(), locationFeatureCollection.getFeatures().size());
  }

  @Test
  @DisplayName(
      "static fromReports(): contains one LocationFeature per distinct Location among " + "the " + "reports")
  void reportsAreFlattenedByLocation() {
    LocationFeatureCollection locationFeatureCollection = LocationFeatureCollection
        .fromReports(reports);

    assertEquals(
        IntegrationTestConfig.FEATURES_COUNT_FOR_KNOWN_DATE,
        locationFeatureCollection.getFeatures().size()
    );
  }

  @Test
  @DisplayName("serializes to GeoJSON FeatureCollection format")
  void serialization() throws JsonProcessingException, JSONException {
    LocationFeatureCollection locationFeatureCollection = LocationFeatureCollection
        .fromReports(reports);

    String serializedFeatureCollection = new ObjectMapper()
        .writeValueAsString(locationFeatureCollection);

    JSONAssert.assertEquals("{ type: FeatureCollection }", serializedFeatureCollection, false);
    JSONAssert.assertNotEquals("{ features: null }", serializedFeatureCollection, false);
  }
}