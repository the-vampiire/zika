package org.launchcode.zikadashboard.models.DTOs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.launchcode.zikadashboard.data.LocationRepository;
import org.launchcode.zikadashboard.data.ReportRepository;
import org.launchcode.zikadashboard.models.Location;
import org.launchcode.zikadashboard.models.Report;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@IntegrationTestConfig
class LocationFeatureTest {
  @Autowired
  private ReportRepository reportRepository;

  @Autowired
  private LocationRepository locationRepository;

  private List<Report> knownDateReports;

  @BeforeEach
  void setup() {
    if (knownDateReports != null) return;

    knownDateReports = reportRepository
        .findAllByReportDate(Date.valueOf(IntegrationTestConfig.KNOWN_REPORTS_DATE));
  }

  @Test
  @DisplayName(
      "static fromLocation(): creates a LocationFeature from a Location object and sets " +
          "properties.reports with all of its associated reports")
  void fromLocation() {
    Location location = locationRepository.getOne(IntegrationTestConfig.KNOWN_LOCATION_ID);

    LocationFeature locationFeature = LocationFeature.fromLocation(location);

    assertEquals(
        IntegrationTestConfig.REPORTS_COUNT_FOR_KNOWN_LOCATION,
        locationFeature.getProperties().getReports().size()
    );
  }

  @Test
  @DisplayName("static fromReport(): creates a LocationFeature from a single Report")
  void fromReport() {
    Report report = knownDateReports.get(0);
    LocationFeature locationFeature = LocationFeature.fromReport(report);
    List<Report> reports = locationFeature.getProperties().getReports();

    assertEquals(1, reports.size());
    assertEquals(report.getId(), reports.get(0).getId());
  }

  @Test
  @DisplayName(
      "static fromReports(): separates reports into a list of LocationFeatures for each distinct "
          + "Location among them")
  void fromReports() {
    List<LocationFeature> locationFeatures = LocationFeature.fromReports(knownDateReports);
    assertEquals(IntegrationTestConfig.FEATURES_COUNT_FOR_KNOWN_DATE, locationFeatures.size());
  }

  @Test
  @DisplayName("addReport(): adds a report to properties.reports")
  void addReport() {
    LocationFeature locationFeature = LocationFeature.fromReport(knownDateReports.get(0));
    locationFeature.addReport(knownDateReports.get(1));

    assertEquals(2, locationFeature.getProperties().getReports().size());
  }

  @Test
  @DisplayName("serializes to GeoJSON Feature format with properties { country, state, reports[] }")
  void serialization() throws JsonProcessingException, JSONException {
    LocationFeature locationFeature = LocationFeature.fromReport(knownDateReports.get(0));
    String serializedLocationFeature = new ObjectMapper().writeValueAsString(locationFeature);
    JSONObject jsonLocationFeatureProperties = new JSONObject(serializedLocationFeature)
        .getJSONObject("properties");

    // test GeoJSON Feature format
    JSONAssert.assertEquals("{ type: Feature }", serializedLocationFeature, false);
    JSONAssert.assertNotEquals("{ geometry: null }", serializedLocationFeature, false);
    assertNotNull(jsonLocationFeatureProperties);

    // test properties shape
    assertTrue(jsonLocationFeatureProperties.has("country"));
    assertTrue(jsonLocationFeatureProperties.has("state"));
    assertTrue(jsonLocationFeatureProperties.has("reports"));
  }

  @Test
  @DisplayName("LocationFeature properties.reports[].location is excluded from serialization")
  void reportsExcludeLocation() throws JsonProcessingException, JSONException {
    LocationFeature locationFeature = LocationFeature.fromReport(knownDateReports.get(0));
    String serializedLocationFeature = new ObjectMapper().writeValueAsString(locationFeature);
    // get an individual serialized report to test for exclusion
    JSONObject jsonReport = new JSONObject(serializedLocationFeature).getJSONObject("properties")
                                                                     .getJSONArray("reports")
                                                                     .getJSONObject(0);

    assertFalse(jsonReport.has("location"));
  }
}