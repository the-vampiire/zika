package org.launchcode.zikadashboard.data;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.zikadashboard.IntegrationTestConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@IntegrationTestConfig
class ReportRepositoryTest {
  @Autowired
  private ReportRepository reportRepository;

  @Test
  @DisplayName("findAllByReportDate(knownDate): returns a List of Reports")
  void getReportsByKnownDate() {
    Date date = Date.valueOf(IntegrationTestConfig.KNOWN_REPORTS_DATE);

    assertEquals(
        IntegrationTestConfig.REPORTS_COUNT_FOR_KNOWN_DATE,
        reportRepository.findAllByReportDate(date).size()
    );
  }

  @Test
  @DisplayName("findAllByReportDate(unknownDate): returns an empty List")
  void getReportsByUnknownDate() {
    Date date = Date.valueOf(IntegrationTestConfig.UNKNOWN_REPORTS_DATE);

    assertEquals(0, reportRepository.findAllByReportDate(date).size());
  }

  @Test
  @DisplayName(
      "findAllDistinctReportDates(): returns a List of all distinct Report dates in " +
          "ascending order")
  void getAllReportDates() {
    List<Date> reportDates = reportRepository.findAllDistinctReportDates();

    assertEquals(IntegrationTestConfig.DISTINCT_REPORT_DATES_COUNT, reportDates.size());

    // check subset sample for correct order
    for (int i = 0; i < IntegrationTestConfig.DISTINCT_REPORT_DATES_COUNT / 4; ++i) {
      assertEquals(-1, reportDates.get(i).compareTo(reportDates.get(i + 1)));
    }
  }
}