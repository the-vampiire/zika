package org.launchcode.zikadashboard;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class EsConfig {
  @Value("${es.index-name}")
  private String indexName;
}
