package org.launchcode.zikadashboard.services;

import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.launchcode.zikadashboard.data.LocationDocumentRepository;
import org.launchcode.zikadashboard.data.LocationRepository;
import org.launchcode.zikadashboard.models.DTOs.LocationFeatureCollection;
import org.launchcode.zikadashboard.models.Location;
import org.launchcode.zikadashboard.models.LocationDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LocationDocumentService {
  @Autowired
  private LocationDocumentRepository locationDocumentRepository;

  @Autowired
  private LocationRepository locationRepository;

  public LocationFeatureCollection search(LocationDocument.SearchTerm term, String value) {
    FuzzyQueryBuilder queryBuilder = QueryBuilders.fuzzyQuery(term.toString(), value);

    ArrayList<Location> locations = new ArrayList<>();
    locationDocumentRepository.search(queryBuilder).forEach(locationDocument -> {
      Location location = locationRepository.getOne(locationDocument.getLocationRowId());
      locations.add(location);
    });

    return LocationFeatureCollection.fromLocations(locations);
  }
}
