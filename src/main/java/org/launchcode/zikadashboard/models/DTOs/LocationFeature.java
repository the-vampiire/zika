package org.launchcode.zikadashboard.models.DTOs;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.launchcode.zikadashboard.models.Location;
import org.launchcode.zikadashboard.models.Report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
public class LocationFeature {
  private final String type = "Feature";

  private LocationFeatureProperties properties;

  @JsonSerialize(using = GeometrySerializer.class)
  private Geometry geometry;

  private LocationFeature(Report report) {
    Location reportLocation = report.getLocation();
    this.setGeometry(reportLocation.getGeometry());
    this.setProperties(new LocationFeatureProperties(reportLocation));

    this.addReport(report);
  }

  private LocationFeature(Location location) {
    this.setGeometry(location.getGeometry());
    this.setProperties(new LocationFeatureProperties(location));
    this.properties.setReports(location.getReports());
  }

  public static LocationFeature fromLocation(Location location) {
    return new LocationFeature(location);
  }

  public static LocationFeature fromReport(Report report) {
    return new LocationFeature(report);
  }

  /**
   * Flattens the reports with a common Location into a LocationFeature
   *
   * <li>eliminates the duplication of Locations (and their hefty geometries) across a set of
   * reports
   *
   * <li> reports are accessible through LocationFeature{}.properties.reports
   *
   * @param reports reports across one or more Locations
   * @return a list of LocationFeatures for each distinct Location among the reports
   * @apiNote LocationFeature: a GeoJSON Feature spec compliant DTO for a Location and its Reports
   *
   * <li> top level LocationFeature{}.geometry holds the common Location geometry of associated
   * Reports
   *
   * <li> LocationFeature{}.properties holds a common .country and .state
   *
   * <li> associated Reports are accessible through LocationFeature{}.properties.reports
   */
  public static List<LocationFeature> fromReports(List<Report> reports) {
    if (reports.size() == 1) {
      // if there is only one report shortcut with a single element list
      List<LocationFeature> locationFeatures = new ArrayList<>();
      locationFeatures.add(LocationFeature.fromReport(reports.get(0)));

      return locationFeatures;
    }

    // use a map for fast lookups in loop
    // create one entry for each distinct Location to separate the reports
    Map<Location, LocationFeature> locationFeatureMap = new HashMap<>();

    for (Report report : reports) {
      Location reportLocation = report.getLocation();
      LocationFeature locationFeature = locationFeatureMap.get(reportLocation);

      if (locationFeature == null) {
        locationFeatureMap.put(reportLocation, LocationFeature.fromReport(report));
      } else {
        locationFeature.addReport(report);
      }
    }

    // output the LocationFeature values as a list for supporting GeoJSON FeatureCollection spec
    return new ArrayList<>(locationFeatureMap.values());
  }

  public void addReport(Report report) {
    this.properties.getReports().add(report);
  }
}

@Data
class LocationFeatureProperties {
  private String country;

  private String state;

  private List<Report> reports;

  LocationFeatureProperties(Location reportLocation) {
    this.country = reportLocation.getCountry();
    this.state = reportLocation.getState();
    this.reports = new ArrayList<>();
  }
}