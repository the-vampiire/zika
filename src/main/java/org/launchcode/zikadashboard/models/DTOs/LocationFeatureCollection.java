package org.launchcode.zikadashboard.models.DTOs;

import lombok.Data;
import org.launchcode.zikadashboard.models.Location;
import org.launchcode.zikadashboard.models.Report;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class LocationFeatureCollection {
  private final String type = "FeatureCollection";

  private List<LocationFeature> features;

  // force use of static methods
  private LocationFeatureCollection() {}

  // static factory methods for instantiating / populating from data sources
  public static LocationFeatureCollection fromReports(List<Report> reports) {
    LocationFeatureCollection locationFeatureCollection = new LocationFeatureCollection();

    locationFeatureCollection.features = LocationFeature.fromReports(reports);

    return locationFeatureCollection;
  }

  public static LocationFeatureCollection fromLocations(List<Location> locations) {
    LocationFeatureCollection locationFeatureCollection = new LocationFeatureCollection();
    locationFeatureCollection.features = locations.stream().map(LocationFeature::fromLocation)
                                                  .collect(Collectors.toList());

    return locationFeatureCollection;
  }
}
