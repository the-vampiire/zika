package org.launchcode.zikadashboard.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "reports")
public class Report {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @ManyToOne
  @JsonIgnore // do not serialize location
  private Location location;

  // format unix timestamp to date string
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private Date reportDate;

  private Double value;

  private String unit;

  private String locationType;

  private String dataField;

  private String dataFieldCode;

  private String timePeriod;

  private String timePeriodType;
}
