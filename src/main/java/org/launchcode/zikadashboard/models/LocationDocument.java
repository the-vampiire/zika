package org.launchcode.zikadashboard.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
// fix for: org.springframework.data.elasticsearch.ElasticsearchException: failed to map source
@NoArgsConstructor
@Document(indexName = "#{@esConfig.getIndexName()}", type = "locations")
public class LocationDocument {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;

  private String country;

  private String state;

  private Long locationRowId;

  private LocationDocument(String country, String state, Long locationRowId) {
    this.country = country;
    this.state = state;
    this.locationRowId = locationRowId;
  }

  public static LocationDocument fromLocation(Location location) {
    return new LocationDocument(location.getCountry(), location.getState(), location.getId());
  }

  public enum SearchTerm {country, state}
}



