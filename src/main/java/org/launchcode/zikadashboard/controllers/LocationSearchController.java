package org.launchcode.zikadashboard.controllers;

import org.launchcode.zikadashboard.models.DTOs.LocationFeatureCollection;
import org.launchcode.zikadashboard.models.LocationDocument;
import org.launchcode.zikadashboard.services.LocationDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"${cors.client-origin}"})
@RequestMapping(value = LocationSearchController.ROOT_ENDPOINT)
public class LocationSearchController {
  public static final String ROOT_ENDPOINT = "/reports/search";

  public static final String SEARCH_TERMS_PATH = "/terms";

  @Autowired
  private LocationDocumentService locationDocumentService;

  @GetMapping
  public LocationFeatureCollection searchByTerm(
      @RequestParam LocationDocument.SearchTerm term, @RequestParam String value
  ) {
    return locationDocumentService.search(term, value);
  }

  @GetMapping(value = SEARCH_TERMS_PATH)
  public LocationDocument.SearchTerm[] searchTerms() {
    return LocationDocument.SearchTerm.values();
  }
}
