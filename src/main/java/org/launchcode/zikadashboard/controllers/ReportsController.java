package org.launchcode.zikadashboard.controllers;

import org.launchcode.zikadashboard.data.LocationDocumentRepository;
import org.launchcode.zikadashboard.data.ReportRepository;
import org.launchcode.zikadashboard.models.DTOs.LocationFeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = {"${cors.client-origin}"})
@RequestMapping(value = ReportsController.ROOT_ENDPOINT)
public class ReportsController {
  public static final String ROOT_ENDPOINT = "/reports";

  public static final String DISTINCT_DATES_PATH = "/dates";

  // simple caching for report dates since they do not change
  private List<Date> reportDates = null;

  @Autowired
  private ReportRepository reportRepository;

  @Autowired
  private LocationDocumentRepository locationDocumentRepository;

  @GetMapping
  public ResponseEntity getReports(@RequestParam(name = "date") String dateString) {
    try {
      LocationFeatureCollection features = LocationFeatureCollection
          .fromReports(reportRepository.findAllByReportDate(Date.valueOf(dateString)));

      return ResponseEntity.ok(features);
    } catch (IllegalArgumentException error) {
      return ResponseEntity.badRequest().body("Invalid Date format, expected YYYY-MM-DD");
    }
  }

  @GetMapping(value = DISTINCT_DATES_PATH)
  public List<Date> getReportDatesList() {
    if (reportDates == null) {
      // simple caching for report dates since they do not change
      reportDates = reportRepository.findAllDistinctReportDates();
    }

    return reportDates;
  }
}
