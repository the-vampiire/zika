package org.launchcode.zikadashboard.data;

import org.launchcode.zikadashboard.models.LocationDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface LocationDocumentRepository extends ElasticsearchRepository<LocationDocument,
    String> {}
