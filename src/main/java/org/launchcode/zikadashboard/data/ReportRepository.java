package org.launchcode.zikadashboard.data;

import org.launchcode.zikadashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
  List<Report> findAllByReportDate(Date date);

  @Query(value = "SELECT DISTINCT report_date FROM reports ORDER BY report_date ASC",
      nativeQuery = true)
  List<Date> findAllDistinctReportDates();
}
