package org.launchcode.zikadashboard;

import org.launchcode.zikadashboard.data.LocationDocumentRepository;
import org.launchcode.zikadashboard.data.LocationRepository;
import org.launchcode.zikadashboard.models.LocationDocument;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ElasticSearchSeeder implements CommandLineRunner {
  private LocationRepository locationRepository;

  private LocationDocumentRepository locationDocumentRepository;

  public ElasticSearchSeeder(
      LocationRepository locationRepository, LocationDocumentRepository locationDocumentRepository
  ) {
    this.locationRepository = locationRepository;
    this.locationDocumentRepository = locationDocumentRepository;
  }

  public static void seed(
      LocationRepository locationRepository, LocationDocumentRepository locationDocumentRepository
  ) {
    List<LocationDocument> locationDocuments = locationRepository.findAll().stream().map(
        LocationDocument::fromLocation).collect(Collectors.toList());

    locationDocumentRepository.saveAll(locationDocuments);
  }

  @Override
  public void run(String... args) {
    boolean shouldSeed = locationDocumentRepository.count() != locationRepository.count();

    if (shouldSeed) {
      System.out.println("\n\n-----------CLEARING ELASTICSEARCH-----------\n\n");
      locationDocumentRepository.deleteAll();
      System.out.println("\n\n-----------STARTING SEEDING-----------\n\n");
      ElasticSearchSeeder.seed(locationRepository, locationDocumentRepository);
      System.out.println("\n\n-----------FINISHED SEEDING-----------\n\n");
    } else {
      System.out.println("\n\n-----------SKIPPING SEEDING-----------\n\n");
    }
  }
}
